import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

public class S2 extends JFrame{
    HashMap sub2 = new HashMap();

    JTextField text1,text2;
    JTextArea text3;
    JButton button;

    S2(){

        setTitle("Interfata");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 20;


        text1 = new JTextField();
        text1.setBounds(70, 50, width, height);

        text2 = new JTextField();
        text2.setBounds(70, 100, width, height);

        text3 = new JTextArea();
        text3.setBounds(70, 150, width, height);
        text3.setEditable(false);

        button = new JButton("Button");
        button.setBounds(10, 200, width, height);

        button.addActionListener(new TreatButton());

        add(text1);
        add(text2);
        add(text3);
        add(button);
    }
    public static void main(String[] args) {
        new S2();
    }

    class TreatButton implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try
            {
            String text1 = S2.this.text1.getText();
            String text2 = S2.this.text2.getText();
            int x=text1.length()+text2.length();
            text3.append(String.valueOf(x));
            }catch(Exception e1){}

        }
    }
}

