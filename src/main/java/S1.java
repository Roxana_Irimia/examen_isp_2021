public class S1 {

    public static void main(String[] args) {
        E e=new E(5);

    }

    public class A(){

    }

    public class B(){
        private String param;
        private C parC;
        private E parE;

        public B(String param,int pC,E parE){
            this.param=param;
            this.parC=new C(pC);
            this.parE=parE;

        }
    }

    public class Z(){
        public void g(){

        }
    }

    public class E(){
        private int pE;

        public E(int pE){
            this.pE=pE;
        }

    }

    public class C(){
        private int pC;

        public C(int pC){
            this.pC=pC;
        }

    }

    public class D(){

        public void f(){

        }
    }
}
